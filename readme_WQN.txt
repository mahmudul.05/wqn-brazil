PQDQN source code for Deep Reinforcement Learning can be sought with proper request.

The following source code is given for the ANN purpose.



To use the WQN Matlab Application, please follow the following instructions:

a) Load the 'wqn_Login.m' file
b) Insert the credentials as follows:
Username: wqn
Password: wqn123

c) It will load the WQN_GUI.mlapp application.
d) Click the necesary button to get the results.
e) The flow of the app shoudl be as follows:
	i. Click the IQA/IET/IVA Buttons. After clicking this, it will appear with dropdown list, click each of the dropdown menu to get the necessary information.
	ii. Click the Submit button, this button will show the Resilience and Normalised Resilience Values of the Water Catchment areas based on the pre-selected IQA/IET/IVA values on the application. This values comes from the ANN Network.
